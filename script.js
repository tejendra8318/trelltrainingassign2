//git test in same branch
const body = document.body;

//navbar element
const arr = ["Home",  "World" , "Magazine" ,"Technology" , "Science" , "Health" ,"Sports" , "Arts" , "Fashion", "Food" , "Travel","Politics", "Sports",];
let navbarElement = document.createElement("header");
navbarElement.classList.add("navbar");
navbarElement.innerHTML = `<ul></ul>`;

//populating navbar with categories
arr.map((val) => {
    const singleNavElement = document.createElement("li");
    singleNavElement.innerText = val;
    singleNavElement.addEventListener("click" , () => {
        postContainerElement.innerHTML = `<h2>Loading...</h2>`;
        apiCaller(val);
    })
  navbarElement.firstChild.appendChild(singleNavElement);
});

body.appendChild(navbarElement);

//main heading element
let mainHeadingElement = document.createElement("h1");
mainHeadingElement.innerText = "The Pertinent Times";
mainHeadingElement.classList.add("heading__primary");
body.appendChild(mainHeadingElement);


//post Conteiner element
let postContainerElement = document.createElement("div");
postContainerElement.innerHTML = `<h2>Loading...</h2>`;
postContainerElement.classList.add("container");
body.appendChild(postContainerElement);


//function to insert post into the postContainer
const populatePosts = (data) => {
    postContainerElement.innerHTML = ``;
  for (let i = 0; i < data.length; i++) {
    const { abstract, web_url, headline, pub_date, news_desk,multimedia } = data[i];
 
    let postElement = document.createElement("div");
    postElement.classList.add("post");
    postElement.innerHTML = `<div><h2>${news_desk}</h2>
        <h3>${headline.main}</h3>
        <p>${pub_date}</p>
        <p>${abstract}></p>
        <a href="${web_url}" target="_blank">Continue reading</a></div>
        <img src="https://static01.nyt.com/${multimedia[0].url}" >`;
    postContainerElement.appendChild(postElement);
  }
};

//apiCaller to NYTimes
function apiCaller(category){
  axios
    .get(
      `https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=CcDi7FuShG2CTw5uS6DK38IsAgoTAVAF&fq=news_desk:("${category}")`
    )
    .then((res) => {
      let data = res.data.response.docs;

      populatePosts(data);
    });
};


//start of our app
apiCaller("Technology");
